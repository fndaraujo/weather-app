# WEATHER APPLICATION

A weather application using a [Pirate Weather](https://pirateweather.net/en/latest/)
free access API register.

## Technology

* [ASP.NET CORE 8.0](https://dotnet.microsoft.com/en-us/apps/aspnet)
* [Pirate Weather API](https://github.com/Pirate-Weather/pirateweather)

## License

[MIT](./LICENSE)

## Authors

See [AUTHORS](./AUTHORS) file.

## Contributors

See [CONTRIBUTORS](./CONTRIBUTORS) file.
