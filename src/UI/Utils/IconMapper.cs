namespace UI.Utils;

public static class IconMapper
{
    public static string GetIconFont(string name) => name switch
    {
        "clear-day" => "oi oi-sun",
        "clear-night" => "oi oi-moon",
        "rain" => "oi oi-rain",
        "snow" => "oi oi-snow",
        "sleet" => "oi oi-sleet",
        "wind" => "oi oi-wind",
        "fog" => "oi oi-fog",
        "cloudy" => "oi oi-cloud",
        "partly-cloudy-day" => "oi oi-cloudy",
        "partly-cloudy-night" => "oi oi-cloudy",
        _ => String.Empty
    };
}
