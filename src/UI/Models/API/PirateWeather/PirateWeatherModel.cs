using UI.Models.API.PirateWeather.Daily;
using UI.Models.API.PirateWeather.Flags;
using UI.Models.API.PirateWeather.Hourly;
using UI.Models.API.PirateWeather.Minutely;

namespace UI.Models.API.PirateWeather;

public class PirateWeatherModel
{
    public double Latitude { get; set; }
    public double Longitude { get; set; }
    public string Timezone { get; set; } = String.Empty;
    public double Offset { get; set; }
    public double Elevation { get; set; }
    public CurrentWeatherModel Currently { get; set; } = new();
    public MinuteWeatherModel Minutely { get; set; } = new();
    public HourWeatherModel Hourly { get; set; } = new();
    public DayWeatherModel Daily { get; set; } = new();
    public FlagsModel Flags { get; set; } = new();
}
