namespace UI.Models.API.PirateWeather.Hourly;

public class HourWeatherModel
{
    public string Summary { get; set; } = String.Empty;
    public string Icon { get; set; } = String.Empty;
    public HourWeatherDataModel[]? Data { get; set; }
}
