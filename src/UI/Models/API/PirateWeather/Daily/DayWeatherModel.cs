namespace UI.Models.API.PirateWeather.Daily;

public class DayWeatherModel
{
    public string Summary { get; set; } = String.Empty;
    public string Icon { get; set; } = String.Empty;
    public DayWeatherDataModel[]? Data { get; set; }
}
