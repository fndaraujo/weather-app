namespace UI.Models.API.PirateWeather.Daily;

public class DayWeatherDataModel
{
    public ulong Time { get; set; }
    public string Icon { get; set; } = String.Empty;
    public string Summary { get; set; } = String.Empty;
    public ulong SunriseTime { get; set; }
    public ulong SunsetTime { get; set; }
    public double MoonPhase { get; set; }
    public double PrecipIntensity { get; set; }
    public double PrecipIntensityMax { get; set; }
    public ulong PrecipIntensityMaxTime { get; set; }
    public double PrecipProbability { get; set; }
    public double PrecipAccumulation { get; set; }
    public string PrecipType { get; set; } = String.Empty;
    public double TemperatureHigh { get; set; }
    public ulong TemperatureHighTime { get; set; }
    public double TemperatureLow { get; set; }
    public ulong TemperatureLowTime { get; set; }
    public double ApparentTemperatureHigh { get; set; }
    public ulong ApparentTemperatureHighTime { get; set; }
    public double ApparentTemperatureLow { get; set; }
    public ulong ApparentTemperatureLowTime { get; set; }
    public double DewPoint { get; set; }
    public double Humidity { get; set; }
    public double Pressure { get; set; }
    public double WindSpeed { get; set; }
    public double WindGust { get; set; }
    public ulong WindGustTime { get; set; }
    public double WindBearing { get; set; }
    public double CloudCover { get; set; }
    public double UvIndex { get; set; }
    public ulong UvIndexTime { get; set; }
    public double Visibility { get; set; }
    public double TemperatureMin { get; set; }
    public ulong TemperatureMinTime { get; set; }
    public double TemperatureMax { get; set; }
    public ulong TemperatureMaxTime { get; set; }
    public double ApparentTemperatureMin { get; set; }
    public ulong ApparentTemperatureMinTime { get; set; }
    public double ApparentTemperatureMax { get; set; }
    public ulong ApparentTemperatureMaxTime { get; set; }
}
