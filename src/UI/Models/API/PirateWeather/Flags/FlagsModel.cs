namespace UI.Models.API.PirateWeather.Flags;

public class FlagsModel
{
    public string[]? Sources { get; set; }
    public SourceTimesModel SourceTimes { get; set; } = new();
    public double NearestStation { get; set; }
    public string Units { get; set; } = String.Empty;
    public string Version { get; set; } = String.Empty;
}
