namespace UI.Models.API.PirateWeather.Minutely;

public class MinuteWeatherDataModel
{
    public ulong Time { get; set; }
    public double PrecipIntensity { get; set; }
    public double PrecipProbability { get; set; }
    public double PrecipIntensityError { get; set; }
    public string PrecipType { get; set; } = String.Empty;
}
