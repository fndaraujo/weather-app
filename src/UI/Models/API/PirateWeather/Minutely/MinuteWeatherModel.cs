namespace UI.Models.API.PirateWeather.Minutely;

public class MinuteWeatherModel
{
    public string Summary { get; set; } = String.Empty;
    public string Icon { get; set; } = String.Empty;
    public MinuteWeatherDataModel[]? Data { get; set; }
}
