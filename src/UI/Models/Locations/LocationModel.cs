using System.ComponentModel.DataAnnotations;

namespace UI.Models.Locations;

public class LocationModel
{
    [Required]
    [Range(-90.00, 90.00, ErrorMessage = "{0} should be between {1} and {2} degrees.")]
    public double Latitude { get; set; }
    [Required]
    [Range(-180.00, 180.00, ErrorMessage = "{0} should be between {1} and {2} degrees.")]
    public double Longitude { get; set; }
}
