namespace ViewModels.Locations.GeodeticCoordinates;

public class GeodeticCoordinatesViewModel
{
    public double Latitude { get; set; }
    public double Longitude { get; set; }
}
