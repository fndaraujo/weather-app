using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace BlazorAppFunctionalTests;

public class HomePageFunctionalTests
{
    [Fact]
    public void GivenAccessTheHomePage_WhenEnterValidGeodeticCoordinates_ThenShowWeatherConditions()
    {
        // As a common user, I can access the weather application home page
        // through its URL string.
        var webDriverOptions = new FirefoxOptions()
        {
            AcceptInsecureCertificates = true
        };
        using IWebDriver webDriver = new FirefoxDriver(
            options: webDriverOptions
        );
        const string uriString = "https://localhost:7088/";
        var homePageUrl = new Uri(uriString: uriString);
        webDriver.Navigate().GoToUrlAsync(url: homePageUrl);

        // I can see that the home page title is "Weather Forecast" in the title
        // bar of the browser.
        string homePageTitle = webDriver.Title;
        const string homePageTitleExpected = "Weather Forecast";
        string homePageTitleActual = homePageTitle;
        Assert.Equal(
            expected: homePageTitleExpected,
            actual: homePageTitleActual
        );

        // I also can see that there is a form to enter geodetic coordinates
        // data on the page.
        const string geodeticCoordinatesFormId = "geodetic-coordinates";
        var geodeticCoordinatesFormWebElement = webDriver
            .FindElement(by: By.Id(idToFind: geodeticCoordinatesFormId));

        // In this form, I can see that there are two input fields for Latitude
        // and Longitude values, and a button, with a "GET" text in it, to
        // submit the values through the form.
        const string latitudeInputId = "latitude";
        var latitudeInputWebElement = webDriver
            .FindElement(by: By.Id(idToFind: latitudeInputId));
        const string longitudeInputId = "longitude";
        var longitudeInputWebElement = webDriver
            .FindElement(by: By.Id(idToFind: longitudeInputId));
        const string geodeticCoordinatesFormButtonId = "geodetic-coordinates-submit-button";
        var geodeticCoordinatesFormButtonWebElement = webDriver
            .FindElement(by: By.Id(idToFind: geodeticCoordinatesFormButtonId));
        const string geodeticCoordinatesFormButtonText = "GET";
        const string geodeticCoordinatesFormButtonTextExpected = geodeticCoordinatesFormButtonText;
        var geodeticCoordinatesFormButtonTextActual = geodeticCoordinatesFormButtonWebElement.Text;
        Assert.Equal(
            expected: geodeticCoordinatesFormButtonTextExpected,
            actual: geodeticCoordinatesFormButtonTextActual
        );
        const string geodeticCoordinatesFormContentsExpected =
            $"Latitude\nLongitude\n{geodeticCoordinatesFormButtonTextExpected}";
        var geodeticCoordinatesFormContentsActual = geodeticCoordinatesFormWebElement.Text;
        Assert.Equal(
            expected: geodeticCoordinatesFormContentsExpected,
            actual: geodeticCoordinatesFormContentsActual
        );

        // If no location was entered, the page shows an invitation to enter a
        // Latitude and a Longitude value in the form.
        const string geodeticCoordinatesNoDataSectionId = "geodetic-coordinates-no-data-section";
        var geodeticCoordinatesNoDataSectionWebElement = webDriver.FindElement(
            by: By.Id(idToFind: geodeticCoordinatesNoDataSectionId)
        );
        var geodeticCoordinatesNoDataSectionIsEnabled = geodeticCoordinatesNoDataSectionWebElement
            .Displayed;
        Assert.True(condition: geodeticCoordinatesNoDataSectionIsEnabled);

        // I can enter a valid Latitude value in the corresponding input field.
        const string latitudeInputValue = "38.575764";
        latitudeInputWebElement.Clear();
        latitudeInputWebElement.SendKeys(text: latitudeInputValue);

        // I can enter a valid Longitude value in its corresponding input field
        // too.
        const string longitudeInputValue = "-121.478851";
        longitudeInputWebElement.Clear();
        longitudeInputWebElement.SendKeys(text: longitudeInputValue);

        // Then, if I click the "GET" button on the form, the page will show
        // weather conditions information corresponding to the geographic
        // location of the Latitude and Longitude entered.
        geodeticCoordinatesFormButtonWebElement.Click();
        Assert.Throws<NoSuchElementException>(testCode: () =>
        {
            geodeticCoordinatesNoDataSectionWebElement = webDriver.FindElement(
                by: By.Id(idToFind: geodeticCoordinatesNoDataSectionId)
            );
        });
        const string weatherConditionsGeneralSectionId = "weather-general-section";
        var weatherConditionsGeneralSectionWebElement = webDriver
            .FindElement(by: By.Id(idToFind: weatherConditionsGeneralSectionId));
        var weatherConditionGeneralSectionContents = weatherConditionsGeneralSectionWebElement.Text;
        Assert.NotEmpty(weatherConditionGeneralSectionContents);

        // With this information, I can leave the page.
        webDriver.Quit();
    }
}
