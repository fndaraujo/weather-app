using ViewModels.Locations.GeodeticCoordinates;

namespace ViewModelsUnitTests;

public class GeodeticCoordinatesModelUnitTests
{
    [Fact]
    public void GeodeticCoordinatesIsInstantiable_ShouldPass()
    {
        // Arrange.
        var geodeticCoordinatesModel = new GeodeticCoordinatesViewModel();

        // Act & Assert.
        Assert.IsType<GeodeticCoordinatesViewModel>(geodeticCoordinatesModel);
    }

    [Fact]
    public void PropertyLatitudeIsNotNull_ShouldPass()
    {
        // Arrange.
        var geodeticCoordinatesModel = new GeodeticCoordinatesViewModel();

        // Act.
        const double expected = 0;
        var latitudeValue = geodeticCoordinatesModel.Latitude;
        var actual = latitudeValue;

        // Arrange.
        Assert.Equal(expected: expected, actual: actual);
    }

    [Fact]
    public void PropertyLongitudeIsNotNull_ShouldPass()
    {
        // Arrange.
        var geodeticCoordinatesModel = new GeodeticCoordinatesViewModel();

        // Act.
        const double expected = 0;
        var longitudeValue = geodeticCoordinatesModel.Longitude;
        var actual = longitudeValue;

        // Arrange.
        Assert.Equal(expected: expected, actual: actual);
    }
}
